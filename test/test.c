#include <stdio.h>
#include <assert.h>

void isr1();
void isr2();

int x = 1;
int y;
int z;
int r = 0;

void GetResource(int r) {
	r = 1;
}

void ReleaseResource(int r) {
	r = 0;
}

void isr1() {
	 x = 1;
	y = 2;
	z = 3;
	
}

void isr2() {
	GetResource(r);
	x++;
	
	x--;
	ReleaseResource(r);
	
}

int main() {
	y=0;
 
    x=1;
    
    y=x;
   
    return 0;
}
