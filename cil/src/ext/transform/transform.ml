open Cil
open Pretty
open Trace
open Feature

module E = Errormsg
module H = Hashtbl

let phase = ref false
let randFuncName = ref "rand"
let addRandProto = ref false

let lu = locUnknown

let randfunc : varinfo option ref = ref None 


(*给定前缀和整个字符串得到它的后缀形式*)
let getSuffix (prefix : string) (s: string) : string =
	let prefixLen = (String.length prefix) in
	let sLen = (String.length s) in
	let cond = (sLen > prefixLen) && (String.sub s 0 prefixLen) = prefix in
	if cond then 
		String.sub s prefixLen (sLen - prefixLen)
	else
		raise (Failure "it's not a valid prefix \n")

let preProcess (f : file) : unit =
	f.globals <- (GText ("#include <stdlib.h>\n\n")) :: f.globals

let rec getFuncList (gl : global list) : fundec list =
	match gl with
	| [] -> []
	| GFun(fd,_) :: rest -> fd ::  getFuncList rest
	|  _ :: rest -> getFuncList rest

(*find the function name whose prefix is fname and then sort *)
let sortFunctionByName (gl : global list) (fname : string) : fundec list =
	let funcList = getFuncList gl in 
	let rec compFunction (fl : fundec list) (fname : string) : fundec list =
		List.filter (fun fd -> startsWith fname fd.svar.vname) fl 
	in let new_fl = compFunction funcList fname in 
	List.sort (fun e1 e2 -> 
		let int_cmp s = (int_of_string (getSuffix fname s)) in
        compare (int_cmp e1.svar.vname) (int_cmp e2.svar.vname)) new_fl

let get_next_fundec (fl : fundec list) (fd : fundec) (prefixName : string) : fundec =
	List.find (fun f -> let int_cmp s = (int_of_string (getSuffix prefixName s)) in
			let string_cmp = compare (int_cmp f.svar.vname) (int_cmp fd.svar.vname) in
				string_cmp > 0) fl

(*produce global function variable - irpt . eg. void isr1() *)
let makeIrptFunction (irptName : string) : varinfo = 
	let v = makeGlobalVar irptName (TFun(voidType,Some [],true,[])) in
	v

(*produce an instruction whose name is irptName*)
let makeIrpt (irptName : string) (args : exp list) : instr =
	let p : varinfo = makeIrptFunction irptName in
	Call (None,Lval(var p),args,!currentLoc)

(*produce rand function variable whose name is rand*)
let makeRandFunction () : varinfo = 
	match !randfunc with
	  Some v -> v
	| None -> begin
		let v = makeGlobalVar !randFuncName (TFun(intType,Some [],true,[])) in
		randfunc := Some v;
		addRandProto := true;
		v
	end

(*produce an instruction whose name is int rand...*)
let makeRand (fd : fundec) (args : exp list): instr =
	let p : varinfo = makeRandFunction () in 
	let res : varinfo = makeTempVar fd intType in
	let randinstr : instr = Call (Some (var res),Lval(var p),args,lu) in
	randinstr
	


let makeCondGuard (fd : fundec) (res : varinfo) : exp =
	let two = integer 2 in
	let randmodtwo = BinOp (Mod,(Lval (var res)), two, intType) in
	let e = BinOp (Eq, randmodtwo, zero, intType) in
	e

class transformVistorClass (nextIrptName: string) = object(self)
	inherit nopCilVisitor

	(*Override the method for visiting instructions*)
	method vfunc (f: fundec) =  
	(*遍历具有ISR名字的函数定义在每个语句前面加入该函数调用*)
		let randfunc = makeRand f [] in
 		let irptfunc = makeIrpt nextIrptName [] in
 		let makeIf (fd : fundec) (first : instr) (second : instr) : stmt list = 
 			let firststmt = mkStmt (Instr [first]) in
 			let body = mkBlock [mkStmt (Instr [second])] in
 			let getReturnVal (i : instr) : varinfo =
 				match i with 
 				 Call (Some (Var vi, NoOffset),_,_,_) -> vi
 				| _ -> raise (Failure "not a valid return value") in
 			let res = getReturnVal first in
			let guard = makeCondGuard fd res in
			let ifstmt = mkStmt (If (guard,body,mkBlock [],lu)) in
			[firststmt ; ifstmt] in
		let blockstmts = makeIf f randfunc irptfunc in
 		let rec addStmtForEach (s : stmt list) (l : stmt list) : stmt list =
			match l with 
				[] -> s
			  | [a] -> s @ [a]
			  | hd :: tl -> s @ [hd] @ addStmtForEach s tl 
			in 
		let newstmts = addStmtForEach blockstmts f.sbody.bstmts in
		f.sbody.bstmts <- newstmts ;
		ChangeTo (f) 
end


let currentFuncName : string ref = ref ""

let simpleTransform (f: file) : unit =
	preProcess f;
	let doGlobal = function 
		| GFun (fd,loc) -> 
			currentFuncName := fd.svar.vname;
			let getIrptNameList = sortFunctionByName f.globals "isr" in
			let len = List.length getIrptNameList in 
			if len = 0 then 
				()
			else begin
				if !currentFuncName = "main" then
				    let nextIrpt = List.hd getIrptNameList in
				    let nextIrptName = nextIrpt.svar.vname in
			        let vis = new transformVistorClass nextIrptName in
				    ignore (visitCilFunction vis fd)
				else begin
					let isExist = List.exists (fun f -> f.svar.vname = !currentFuncName) getIrptNameList in
					if isExist then
						let pos = len - 1 in 
						let lastIrpt = List.nth getIrptNameList pos  in
						let lastIrptName = lastIrpt.svar.vname in 
						if !currentFuncName = lastIrptName then 
							()
						else 
							let nextIrpt = get_next_fundec getIrptNameList fd "isr" in 
							let nextIrptName = nextIrpt.svar.vname in
							let vis = new transformVistorClass nextIrptName in
							ignore (visitCilFunction vis fd)
					else
						()
				end
			end
		| _ -> ()
	 in Stats.time "tranform" (iterGlobals f) doGlobal

let feature = {
	fd_name = "tranform";
	fd_enabled = true;
	fd_description = "Generation of code to produce function calls at every statement";
	fd_extraopt = [];
	fd_doit = simpleTransform;
	fd_post_check = true
}

let () = Feature.register feature

