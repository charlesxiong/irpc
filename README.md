# ** irpt-racecheck** 
Data Race Detection Implementation for Interrupt-Driven C Programs in Ocaml

---

# ** Installation**
To build and install CIL, you need the Ocaml compiler perl and ocamlfind. (Of course,you also need gcc). Run the commands(Tested On Ubuntu 14.04-64bit platform  )

```Shell
sudo apt-get install ocaml
sudo apt-get install opam 
opam install ocamlfind
```

Run the following commands to build CIL and use the feature transform(/cil/src/ext/transform/transform.ml):

```
./configure
make # using the transformation feature by default
./idpc [options] # the main execution
make install # optional, no need
```

---

# ** Usage**

For example,if you want to check the interrupt-driven programs for data race :
* First,you need choose the feature `transform` by default and transform your program into non-deterministic sequential program
* Second,use the data race analysis

# ** Current Goals**
- Interrupt-driven programs transformation into non-deterministic sequential programs
- Building a general AST from different embedding system interrupt-driven program
- Continue..

---


# ** To Do** 

* 2015.05.15-2015.07.31

1. Simpler concurrent program into sequential programs
2. Add command-line option to the executable code

# 目前进展
1. 完成中断C程序到非确定性的程序转换（仍有一些细节需要考虑）
代码实现在`/cil/src/ext/tranform/transform.ml`中

存在或需要改进的问题:
a. 关于如何确定中断任务。根据嵌入式程序的特点，有可能不好识别中断的函数，可让程序读取一个配置文件再左
b. C程序的简化,要保证转换后的程序只有基本的赋值语句和函数调用，如果有循环和或者条件，需要先循环展开
b. 关于随机生成概率的问题

---
